package br.edu.ifsc.mudatexto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;

import java.util.concurrent.ThreadLocalRandom;

public class MainActivity extends AppCompatActivity {

     EditText editText1;
     EditText editText2;
     TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText1 = findViewById(R.id.editText1);
        editText2 = findViewById(R.id.editText2);
        textView = findViewById(R.id.textView);

    }

    public void sortear(View view) {
        int randomNum = ThreadLocalRandom.current().nextInt(Integer.parseInt(editText1.getText().toString()), Integer.parseInt(editText2.getText().toString()));
        textView.setText(Integer.toString(randomNum));
    }

}